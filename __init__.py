import re

pattern = ('[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}')

if __name__ == "__main__":

    correo = input("Ingrese un correo ")
    if re.match(pattern, correo) is not None:
        print(" El Correo es valido ")
    else:
        print("Correo invalido")
